const micro = require('micro')
const puppeteer = require('puppeteer')
const parse = require('url')

async function pageToPDF(req, res) {
  const url = parse.parse(req.url, true).query.url

  if (!url) {
    res.statusCode = 400
    res.end()
    return
  }

  const browser = await puppeteer.launch({dumpio: true})
  const page = await browser.newPage()
  
  await page.goto(url, { waitUntil: 'networkidle' })
  const pdf = await page.pdf({format: 'A4'})

  browser.close()

  res.statusCode = 200
  res.setHeader('Content-Type', 'application/pdf')
  res.setHeader('Content-Length', pdf.length)
  res.setHeader('Connection', 'close')
  res.end(pdf)
}

const server = micro(pageToPDF)
server.listen(process.env.PORT || 1337)